package us.fetchr.newrelicsdk.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import us.fetchr.ordertrackingcommons.enums.OrderTrackingActions;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class NewRelicAttributes {

    private String trackingNumber;
    private OrderTrackingActions name;

}
