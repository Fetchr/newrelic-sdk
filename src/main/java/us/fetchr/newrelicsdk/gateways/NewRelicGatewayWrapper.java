package us.fetchr.newrelicsdk.gateways;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.newrelic.api.agent.NewRelic;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import us.fetchr.newrelicsdk.models.NewRelicAttributes;

import java.util.Map;

@Component
@Log4j2
public class NewRelicGatewayWrapper<T extends NewRelicAttributes> {

    private static String EVENT = "event";
    private ObjectMapper mapper = new ObjectMapper();

    public void record(T attributes) {
        record(attributes, EVENT);
    }

    public void record(T attributes, String eventName) {

        if (attributes == null) {
            log.info("No need to record for this event");
            return;
        }

        try {
            Map eventAttrs = mapper.readValue(mapper.writeValueAsString(attributes), Map.class);
            NewRelic.getAgent().getInsights().recordCustomEvent(eventName, eventAttrs);
        } catch (Exception e) {
            log.error("Failed to record new relic", e);
        }
    }
}
